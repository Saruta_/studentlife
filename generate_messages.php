<?php
	session_start();
	$id = $_SESSION['id'];
	if(isset($id)){
		include_once('modele/connexion_sql.php');
		include_once('modele/get_messages.php');
		include_once('modele/get_profil.php');
		include_once('modele/set_time.php');
		$messages = getMessages(40);
		$profil = getProfil($id);
		setCurrentTime($profil[0]['id']);

	?>
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8"/>
		</head>
		<body>
		<ul style="list-style-type: none;">
	<?php 
		foreach($messages as $cle => $message){
			?><li><?php 
			$profil_messages = getProfil($messages[$cle]['pseud_id']); 
			?><span style="color: #310c53;
							font-weight: bold;">
			<?php echo $profil_messages[0]['prenom'] . ': '; ?> </span> <?php
			$messages[$cle]['message'] = htmlentities($message['message']);
			echo $messages[$cle]['message']; ?></li> <?php
		}
		?>
	</ul></body></html><?php
	}
	
