<?php
	session_start();



	if(isset($_SESSION['id']) AND isset($_SESSION['pseudo'])){
		include_once('modele/connexion_sql.php');
		
		if(!empty($_GET['page']) && is_file('controleurs/' . $_GET['page'] . '_controleur.php')){
			?>
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8"/>
				<link rel="icon" type="image/jpg" href="modele/src/icone.jpg"/>
				<link rel="stylesheet" href="style.css"/>
				<link href='http://fonts.googleapis.com/css?family=Mouse+Memoirs' rel='stylesheet' type='text/css'/>
				<title>Student'Life</title>
				<style>
		.icone
		{
			width: 100px;
			height: 100px;
			display: inline-block;
			
		}
		/*#images
		{
			display: block;
		}*/
		#images img
		{
			position: absolute;
		}
		#text
		{
			display: block;
		}
		nav
		{
			position: relative;
			width: 400px;
			margin: 0 auto;
		}
	</style>	
			</head>
			<body>
			<?php
			include_once('vue/header.php');
			include_once('vue/menu.php');
			include_once('controleurs/' . $_GET['page'] . '_controleur.php');
			include_once('vue/numero_version.php');
			?>
			</body>
			</html>
			<?php
		}
		else{
			?>
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8"/>
				<link rel="icon" type="image/jpg" href="modele/src/icone.jpg"/>
				<link rel="stylesheet" href="style.css"/>
				<link href='http://fonts.googleapis.com/css?family=Mouse+Memoirs' rel='stylesheet' type='text/css'/>
				<title>Student'Life</title>
				<style>
		.icone
		{
			width: 100px;
			height: 100px;
			display: inline-block;
			
		}
		/*#images
		{
			display: block;
		}*/
		#images img
		{
			position: absolute;
		}
		#text
		{
			display: block;
		}
		nav
		{
			position: relative;
			width: 400px;
			margin: 0 auto;
		}
	</style>	
			</head>
			<body>
			<?php
			include_once('vue/header.php');
			include_once('vue/menu.php');
			include_once('controleurs/photo_controleur.php');
			include_once('vue/numero_version.php');
			?>
			</body>
			</html>
			<?php
		}
	}
	else{

		header('Location: connexion.php');

	}
