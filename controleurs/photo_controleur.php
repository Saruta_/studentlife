<?php
	session_start();
	$id = $_SESSION['id'];
	if(isset($id)){
		include_once('modele/get_profil.php');
		include_once('modele/get_photo.php');
		include_once('modele/set_time.php');

		setCurrentTime($id);
		$profil = getProfil($id);
		$photo = getPhoto($profil[0]['classe'], $profil[0]['lycee']); 
		$profil_photo = getProfil($photo[0]['pseud_id']);
		$path = $photo[0]['path'];
		$prenom = $profil[0]['prenom'];
		include_once('vue/photo_vue.php');
		//include_once('chat_controleur.php');
	}
	else
	{
		header('Location:connexion.php');
	}
			
	
