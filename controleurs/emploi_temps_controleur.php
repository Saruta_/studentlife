<?php
	session_start();
	$id = $_SESSION['id'];
	if(isset($id)){
		include_once('modele/get_profil.php');
		include_once('modele/get_emploiTemps.php');
		include_once('modele/set_time.php');
		$profil = getProfil($id);
		$emploi_temps_lundi = getEmploiTemps($profil[0]['classe'], $profil[0]['lycee'], 'Lundi');
		$emploi_temps_mardi = getEmploiTemps($profil[0]['classe'], $profil[0]['lycee'], 'Mardi');
		$emploi_temps_mercredi = getEmploiTemps($profil[0]['classe'], $profil[0]['lycee'], 'Mercredi');
		$emploi_temps_jeudi = getEmploiTemps($profil[0]['classe'], $profil[0]['lycee'], 'Jeudi');
		$emploi_temps_vendredi = getEmploiTemps($profil[0]['classe'], $profil[0]['lycee'], 'Vendredi');
		
		setCurrentTime($profil[0]['id']);

	
		foreach($emploi_temps_lundi as $cle => $line){
			$emploi_temps_lundi[$cle]['matiere'] = $line['matiere'];
			$emploi_temps_lundi[$cle]['salle'] = $line['salle'];
			$emploi_temps_lundi[$cle]['prof'] = $line['prof'];
		}
		foreach($emploi_temps_mardi as $cle => $line){
			$emploi_temps_mardi[$cle]['matiere'] = $line['matiere'];
			$emploi_temps_mardi[$cle]['salle'] = $line['salle'];
			$emploi_temps_mardi[$cle]['prof'] = $line['prof'];
		}	
		foreach($emploi_temps_mercredi as $cle => $line){
			$emploi_temps_mercredi[$cle]['matiere'] = $line['matiere'];
			$emploi_temps_mercredi[$cle]['salle'] = $line['salle'];
			$emploi_temps_mercredi[$cle]['prof'] = $line['prof'];
		}
		foreach($emploi_temps_jeudi as $cle => $line){
			$emploi_temps_jeudi[$cle]['matiere'] = $line['matiere'];
			$emploi_temps_jeudi[$cle]['salle'] = $line['salle'];
			$emploi_temps_jeudi[$cle]['prof'] = $line['prof'];
		}
		foreach($emploi_temps_vendredi as $cle => $line){
			$emploi_temps_vendredi[$cle]['matiere'] = $line['matiere'];
			$emploi_temps_vendredi[$cle]['salle'] = $line['salle'];
			$emploi_temps_vendredi[$cle]['prof'] = $line['prof'];
		}	
		include_once('vue/emploi_temps_vue.php');
	}
	else{
		header('Location:connexion.php');
	}
