<?php
	session_start();
	$id = $_SESSION['id'];
	if(isset($id)){
		include_once('modele/connection_sql.php');
		include_once('modele/get_profils.php');
		include_once('modele/get_profil.php');
		include_once('modele/set_time.php');
	
		$profil = getProfil($id);	
		$profils = getProfils($profil[0]['classe'], $profil[0]['lycee']);
		$classe = $profil[0]['classe'];
		$lycee = $profil[0]['lycee'];
		setCurrentTime($profil[0]['id']);

		
		foreach($profils as $cle =>$profil){
			$profils[$cle]['nom'] = htmlspecialchars($profil['nom']);
			$profils[$cle]['prenom'] = htmlspecialchars($profil['prenom']);
			$profils[$cle]['connected'] = htmlspecialchars($profil['connected']);
		}
		include_once('vue/profil_vue.php');
	}
	else{
		header('Location:connexion.php');
	}
