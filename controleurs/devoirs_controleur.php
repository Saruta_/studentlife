<?php
	session_start();
	$id = $_SESSION['id'];
	if(isset($id)){
		include_once('modele/get_devoirs.php');
		include_once('modele/get_profil.php');
		include_once('modele/set_time.php');
		$profil = getProfil($id);
		setCurrentTime($profil[0]['id']);
		$devoirs = getDevoirs($profil[0]['classe'], $profil[0]['lycee']);
		
		foreach($devoirs as $cle =>$devoir){
				$devoirs[$cle]['devoir'] = htmlentities($devoir['devoir']);
				$devoirs[$cle]['matiere'] = htmlentities($devoir['matiere']);
				$devoirs[$cle]['jour'] = htmlentities($devoir['jour']);
			}
		include_once('vue/devoirs_vue.php');
	}
	else
	{
		header('Location:connexion.php');
	}
