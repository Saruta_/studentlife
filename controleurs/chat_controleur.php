<?php
	session_start();
	$id = $_SESSION['id'];	
	if(isset($id)){	
		include_once('modele/get_profil.php');
		include_once('modele/get_messages.php');
		include_once('modele/set_message.php');
		include_once('modele/set_time.php');

		$profil = getProfil($id);
		setCurrentTime($profil[0]['id']);
		if(isset($_POST['message_envoi'])){
			setMessage($id, $_POST['message_envoi']);
		}
		$classe = $profil[0]['classe'];
		
		include_once('vue/chat_vue.php');
	}
	else{
		header('Location:connexion.php');
	}
