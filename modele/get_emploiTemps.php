<?php
	
function getEmploiTemps($classe, $lycee, $jour){
	global $db;
	$table = 'Emploi_temps_' . $classe . '_' .$lycee;	
	$req = $db->prepare('SELECT * FROM ' . $table . ' WHERE jour = ? ORDER BY id ASC');
	$req->execute(array($jour));
	$emploi_temps = $req->fetchAll();
	return $emploi_temps;
}
