<?php

function getPhoto($classe, $lycee){
	global $db;
	$req = $db->prepare('SELECT * FROM Photos WHERE classe = ? AND lycee= ?');
	$req->execute(array($classe, $lycee));
	$photo = $req->fetchAll();
	return $photo;
}
