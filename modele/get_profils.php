<?php

function getProfils($classe, $lycee){ 
	global $db;
	$req = $db->prepare('SELECT * FROM Profils WHERE classe = :classe AND lycee = :lycee ORDER BY time DESC');
	$req->execute(array('classe' => $classe,
				'lycee' => $lycee));
	$profils = $req->fetchAll();
	return $profils;
}

