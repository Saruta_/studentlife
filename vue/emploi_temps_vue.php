<?php
	//functions

	function emploi_temps($jour_p, $emploi_temps_p){
	 ?>
	<div id="<?php echo($jour_p); ?>">
		<table>
			<?php 
		
			foreach($emploi_temps_p as $line){?>
				<tr>
					<td><?php 
					if($line['matiere'] == 'Etude'){
						echo($line['matiere']);
					}
					else{
						echo($line['matiere'] . ' en ' . $line['salle'] . ' avec ' . $line['prof']); 
					}
					?></td>			
				<tr>
			<?php } ?>
		</table>
	</div>
	<?php 
	}
	
?>
	<div class="bloc_get">
	<h2>Emploi du temps</h2>
	<form>
		<select id="jour">
			<option SELECTED>Lundi</option>
			<option >Mardi</option>
			<option >Mercredi</option>
			<option >Jeudi</option>
			<option >Vendredi</option>
		</select>
	</form>
	<?php emploi_temps('Lundi', $emploi_temps_lundi); ?>
	<?php emploi_temps('Mardi', $emploi_temps_mardi); ?>
	<?php emploi_temps('Mercredi', $emploi_temps_mercredi); ?>	
	<?php emploi_temps('Jeudi', $emploi_temps_jeudi); ?>
	<?php emploi_temps('Vendredi', $emploi_temps_vendredi); ?>		
	</div>
	
	
<script src="jquery.js"></script>
<script>
	$(function(){
						$('#Mardi').hide();
						$('#Mercredi').hide();
						$('#Jeudi').hide();
						$('#Vendredi').hide();
				$('select').change(function(){
						var param = '#' + $('select').val();
						$('#Lundi').hide();
						$('#Mardi').hide();
						$('#Mercredi').hide();
						$('#Jeudi').hide();
						$('#Vendredi').hide();
						$(param).show();
				});
		});
</script>

