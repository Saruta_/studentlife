<?php
/*	try{
		$db = new PDO('sqlite:/Users/marcguitar/Sites/php/studentLife/modele/studentLife.db');
	}
	catch(Exception $e){
		die('Erreur : ' . $e->getMessage());	
	}*/
	include_once('modele/connexion_sql.php');

	$username = $_POST['UserName']; 
	$password = $_POST['Password']; 

	$req = $db->prepare('SELECT id FROM Profils WHERE nom = :nom AND pass = :pass');
	$req->execute(array('nom' => $username, 'pass' => $password));
	$result = $req->fetch();
	if(!$result)
	{
		header('Location:connexion.php');	
	}
	else{
		session_start();
		$_SESSION['id'] = $result['id'];	
		$_SESSION['pseudo'] = $username;	
		header('Location: index.php');
	}

