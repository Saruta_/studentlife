<?php
	session_start();
	if(isset($_FILES['image']) AND $_FILES['image']['error'] == 0){
		if($_FILES['image']['size'] < 10000000){
			$info = pathinfo($_FILES['image']['name']);
			$extens_upload = $info['extension'];
			$extens_autori = array('JPG', 'PNG', 'jpg', 'jpeg', 'gif', 'png');
			if(in_array($extens_upload, $extens_autori)){
				//code de pek
				$id = $_SESSION['id'];
				include_once('modele/connexion_sql.php');
				include_once('modele/rm_photo.php');
				include_once('modele/set_photo.php');	
				include_once('modele/get_photo.php');
				include_once('modele/get_profil.php');
				$profil = getProfil($id);
				$photo_old = getPhoto($profil[0]['classe'], $profil[0]['lycee']);
				$path_old = $photo_old[0]['path'];
					
				//Suppression photo + ligne base donnée
				rmPhoto($profil[0]['classe'], $profil[0]['lycee']);
				unlink($path_old);

				//ecriture base de donnée + fichier repertoire + redirection
				setPhoto('modele/photos/' . basename($_FILES['image']['name']), $id, $_POST['commentaire'], $profil[0]['classe'], $profil[0]['lycee']);
				move_uploaded_file($_FILES['image']['tmp_name'], 'modele/photos/' . basename($_FILES['image']['name']));
				header('Location:index.php');
			}
		}
	}
	else{
		header('Location:index.php');
	}	
